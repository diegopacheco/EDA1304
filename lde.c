#include <stdio.h>
#include <stdlib.h>

typedef struct lista{
	int posicao;
	struct lista *proximo;
	struct lista *anterior;
}LISTA;

typedef struct{
	int tamanho;
	struct lista *primeiro;
	struct lista *ultimo;
}CONTROLE;

CONTROLE *geral;

//Prototipos
void inserirElemento ();
void removerElemento ();
void imprimirLista ();

int main(void){
	int opcao;

	geral = (CONTROLE *)malloc(sizeof(CONTROLE));

	geral->tamanho = 0;
	geral->primeiro = NULL;
	geral->ultimo = NULL;

	system("clear");

	do{
		printf("MENU:\n\n");
		printf("0 - SAIR\n");
		printf("1 - INSERIR ELEMENTO\n");
		printf("2 - REMOVER ELEMENTO\n");
		printf("3 - IMPRIMIR LISTA\n");
		printf("\nQUAL OPCAO DESEJA ACESSAR? ");
		scanf("%i",&opcao);

		switch(opcao){
			case 1: inserirElemento();
				break;
			case 2: if(geral->tamanho == 0){
					printf("\nNAO FOI ENCONTRADO NENHUM ELEMENTO!\n\n");
				}else{
				 	removerElemento();
				}
				break;
			case 3: if(geral->tamanho == 0){
					printf("\nNAO FOI ENCONTRADO NENHUM ELEMENTO!\n\n");
				}else{
				 	imprimirLista();
				}
				break;
		}
	}while(opcao != 0);

	return 0;
}

void inserirElemento (){
	LISTA *aux = (LISTA *)malloc(sizeof(LISTA));

	system("clear");

	printf("1 - INSERIR ELEMENTO\n\n");
	printf("\tINSIRA O ELEMENTO: ");
	scanf("%i",&aux->posicao);

	if(geral->tamanho == 0){
		geral->primeiro = aux;
		geral->ultimo = aux;
	}
	else{
		aux->proximo = geral->primeiro;
		geral->primeiro = aux;
		aux = aux->proximo;
		aux->anterior = geral->primeiro;
	}

	geral->tamanho++;
	printf("\nELEMENTO INSERIDO!\n\n");

	return;
}

void removerElemento (){
	int contador = geral->tamanho;
	int remover;
	LISTA *aux, *ajuda;

	system("clear");

	printf("2 - REMOVER ELEMENTO\n\n");
	printf("QUAL ELEMENTO DESEJA REMOVER? ");
	scanf("%i",&remover);

	aux = geral->primeiro;

	if(geral->primeiro == geral->ultimo){
		free(aux);

		geral->primeiro = NULL;
		geral->ultimo = NULL;
		geral->tamanho--;

		printf("\nELEMENTO EXCLUIDO!\n\n");

		return;
	}

	while(contador != 0){
		if(remover == aux->posicao){
			if(geral->primeiro == aux){
			 	geral->primeiro = aux->proximo;
				ajuda = geral->primeiro;
				ajuda->anterior = NULL;
			}
			else{
				aux->anterior->proximo = aux->proximo;
			}

			if(geral->ultimo == aux){
				geral->ultimo = aux->anterior;
				ajuda = geral->ultimo;
				ajuda->proximo = NULL;
			}
			else{
				aux->proximo->anterior = aux->anterior;
			}
			free(aux);

			geral->tamanho--;
			printf("\nELEMENTO EXCLUIDO!\n\n");

			return;
		}
		else{
			aux = aux->proximo;
		}
	}
	printf("\nELEMENTO NAO ENCONTRADO!\n\n");

	return;
}

void imprimirLista (){
	int contador = geral->tamanho;
	LISTA *aux;

	aux = geral->primeiro;

	system("clear");

	printf("3 - IMPRIMIR LISTA\n\n");

	while(contador != 0){
		if(contador != geral->tamanho){
			aux = aux->proximo;
		}

		contador--;

		printf("%d\n", aux->posicao);
	}

	return;
}
