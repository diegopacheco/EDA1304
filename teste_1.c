#include<stdio.h>
#include<stdlib.h>

typedef struct VETOR_LISTA{
    int vetor;
    struct VETOR_LISTA *prx;
    struct VETOR_LISTA *ant;

}LISTA;

typedef struct {
    int tam;
    struct VETOR_LISTA *ini;
    struct VETOR_LISTA *fim;
}CONTROLE;
CONTROLE *geral;

void MENU(){
    printf("--------------------------------------\n");
    printf("SAIR\t\t0\n");
    printf("INSERIR\t\t1\n");
    printf("DELETAR\t\t2\n");
    printf("IMPRIMIR\t3\n");
    printf("--------------------------------------\n");

}

void INSERIR_ELEMENTO(){
    LISTA *aux = (LISTA*)malloc(sizeof(LISTA));
    system("clear");
    printf("--------------------------------------\n");
    printf("INSIRA O Nº: \n");
    scanf("%d",&aux->vetor);
    if (geral->tam ==0) {
        geral->ini = aux;
        geral->fim = aux;
    }
    else {
        aux->prx = geral->ini;
        geral->ini = aux;
        aux = aux->prx;
        aux->ant = geral->ini;
    }
    geral->tam++;
    printf("-------------------------------------\n");
    return;

}

void IMPRIMIR(){
    int contador = geral->tam;
    LISTA *aux;
    aux = geral->ini;
    system("clear");
    printf("------------------------------------\n");
    while(contador!= 0){
        if (contador != geral->tam) {
            aux = aux->prx;
        }
        contador--;
        printf("%d\n",aux->vetor);

    }

}

int main (void)
{
    int opcao;
    geral = (CONTROLE*)malloc(sizeof(CONTROLE));
    geral->tam = 0;
    geral->ini = NULL;
    geral->fim = NULL;
    system("clear");
    do{
        MENU();
        scanf("%d",&opcao);
        switch(opcao){
            case 1:
                INSERIR_ELEMENTO();
                break;
            case 3:
                IMPRIMIR();
                break;
            default:
                break;

        }


    }while(opcao!=0);
    return 0;
}
